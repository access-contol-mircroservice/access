package br.com.kaiquer.access.service;

import br.com.kaiquer.access.clients.door.DoorClient;
import br.com.kaiquer.access.clients.door.exceptions.DoorInvalidException;
import br.com.kaiquer.access.clients.person.PersonClient;
import br.com.kaiquer.access.clients.person.exceptions.PersonInvalidException;
import br.com.kaiquer.access.exceptions.AccessAlreadyTakenException;
import br.com.kaiquer.access.exceptions.AccessNotFoundException;
import br.com.kaiquer.access.models.Access;
import br.com.kaiquer.access.repository.AccessRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccessService {
    @Autowired
    private AccessRepository repository;

    @Autowired
    private PersonClient personClient;

    @Autowired
    private DoorClient doorClient;

    public Access create(Access access) {
        try{
            personClient.findById(access.getPersonId());
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new PersonInvalidException();
        }
        try{
            doorClient.findById(access.getDoorId());
        } catch (FeignException.FeignClientException.NotFound e) {
            throw new DoorInvalidException();
        }

        if(repository.findByPersonAndDoor(access.getPersonId(), access.getDoorId()).isPresent()){
            throw new AccessAlreadyTakenException();
        }

        return this.repository.save(access);
    }

    public void delete(Long personId, Long doorId) {
        Access access = findExistingAcecss(personId,doorId);
        repository.delete(access);
    }

    public Access findExistingAcecss(Long personId, Long doorId){
        Optional<Access> access = repository.findByPersonAndDoor(personId,doorId);
        if(!access.isPresent()) {
            throw new AccessNotFoundException();
        }
        return access.get();
    }
}

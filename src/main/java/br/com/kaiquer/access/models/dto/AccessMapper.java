package br.com.kaiquer.access.models.dto;

import br.com.kaiquer.access.models.Access;
import br.com.kaiquer.access.models.dto.request.AccessCreateRequest;
import br.com.kaiquer.access.models.dto.response.AccessCreatedResponse;
import br.com.kaiquer.access.models.dto.response.AccessDetailResponse;
import org.springframework.stereotype.Component;

@Component
public class AccessMapper {

    public Access toAccess(AccessCreateRequest accessCreateRequest) {
        Access access = new Access();
        access.setDoorId(accessCreateRequest.getDoorId());
        access.setPersonId(accessCreateRequest.getPersonId());

        return access;
    }

    public AccessCreatedResponse toAccessCreatedResponse(Access access) {
        AccessCreatedResponse response = new AccessCreatedResponse();
        response.setDoorId(access.getDoorId());
        response.setPersonId(access.getPersonId());

        return response;
    }

    public AccessDetailResponse toAccessDetailResponse(Access access) {
        AccessDetailResponse response = new AccessDetailResponse();
        response.setDoorId(access.getDoorId());
        response.setPersonId(access.getPersonId());

        return response;
    }
}

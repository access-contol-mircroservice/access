package br.com.kaiquer.access.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessCreateRequest {
    @JsonProperty(value = "cliente_id")
    private Long personId;
    @JsonProperty(value = "porta_id")
    private Long doorId;

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }
}

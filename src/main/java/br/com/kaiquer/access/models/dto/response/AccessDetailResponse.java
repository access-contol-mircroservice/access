package br.com.kaiquer.access.models.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessDetailResponse {

    @JsonProperty(value = "porta_id")
    private Long doorId;
    @JsonProperty(value = "cliente_id")
    private Long personId;

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }

}

package br.com.kaiquer.access.controller;

import br.com.kaiquer.access.models.Access;
import br.com.kaiquer.access.models.dto.AccessMapper;
import br.com.kaiquer.access.models.dto.request.AccessCreateRequest;
import br.com.kaiquer.access.models.dto.response.AccessCreatedResponse;
import br.com.kaiquer.access.models.dto.response.AccessDetailResponse;
import br.com.kaiquer.access.service.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AccessController {
    @Autowired
    private AccessService accessService;

    @Autowired
    private AccessMapper mapper;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public AccessCreatedResponse newAccess(@RequestBody AccessCreateRequest accessCreateRequest) {
        Access access = mapper.toAccess(accessCreateRequest);
        AccessCreatedResponse response = mapper.toAccessCreatedResponse(accessService.create(access));

        return response;
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeAccess(@PathVariable(value = "cliente_id") Long personId, @PathVariable(value = "porta_id") Long doorId) {
        accessService.delete(personId, doorId);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public AccessDetailResponse getAccess(@PathVariable(value = "cliente_id") Long personId, @PathVariable(value = "porta_id") Long doorId){
        return mapper.toAccessDetailResponse(accessService.findExistingAcecss(personId, doorId));
    }

}

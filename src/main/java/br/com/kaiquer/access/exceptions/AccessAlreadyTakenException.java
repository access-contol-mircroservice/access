package br.com.kaiquer.access.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "acesso já concedido")
public class AccessAlreadyTakenException extends RuntimeException {
}

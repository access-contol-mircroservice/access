package br.com.kaiquer.access.clients.door;

import br.com.kaiquer.access.clients.door.dto.Door;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta")
public interface DoorClient {
    @GetMapping("/porta/{id}")
    Door findById(@PathVariable(value = "id") Long id);

}

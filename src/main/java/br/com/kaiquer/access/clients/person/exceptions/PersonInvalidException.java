package br.com.kaiquer.access.clients.person.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "cliente inválido")
public class PersonInvalidException extends RuntimeException {
}

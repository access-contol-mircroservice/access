package br.com.kaiquer.access.clients.person;

import br.com.kaiquer.access.clients.person.dto.Person;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface PersonClient {
    @GetMapping("/cliente/{id}")
    Person findById(@PathVariable(value = "id") Long id);
}

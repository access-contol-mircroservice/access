package br.com.kaiquer.access.clients.door.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "porta inválida")
public class DoorInvalidException extends RuntimeException {
}

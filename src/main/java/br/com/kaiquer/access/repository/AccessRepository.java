package br.com.kaiquer.access.repository;

import br.com.kaiquer.access.models.Access;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccessRepository extends CrudRepository<Access, Long> {
    @Query(value = "SELECT a FROM Access a WHERE a.personId = ?1 AND a.doorId = ?2  ")
    Optional<Access> findByPersonAndDoor(Long personId, Long doorId);
}
